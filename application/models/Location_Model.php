<?php 
   class Location_Model extends CI_Model {
	
      function __construct() { 
         parent::__construct(); 
      } 
   
      public function insert($data) { 
         if ($this->db->insert("location", $data)) { 
            return true; 
         } 
      } 
      
      public function update($data,$address) { 
         $this->db->set($data); 
         $this->db->where("address", $address); 
         $this->db->update("location", $data); 
      } 
   } 
?> 