<?php 
   class Location_controller extends CI_Controller {
	
      function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->database(); 
      } 
  
      public function index() { 
      	 $this->db->where("status", false);
         $query = $this->db->get("location"); 
         $data['records'] = $query->result(); 
			
         $this->load->helper('url'); 
         $this->load->helper('form');
         $this->load->view('Location_view',$data); 
      } 
  
      public function add_location_view() { 
         $this->load->helper('form'); 
         $this->load->view('Location_add'); 
      } 
  
      public function add_location() { 
         $this->load->model('Location_Model');
			
         $data = array( 
            'name' => $this->input->post('name'), 
         	'address' => $this->input->post('address'),
         	'note' => $this->input->post('note'),
         	'status' => false,
         ); 
			
         $this->Location_Model->insert($data); 
   
          $this->load->helper('form');
          $this->load->view('Location_add'); 
      } 
  
      public function update_location(){ 
         $this->load->model('Location_Model');
			
         $data = array( 
         	'status' => true,
         ); 
			
         $arr_selected = $this->input->post('selectedPoint');
         foreach ($_POST['selectedPoint'] as $address)
         {
         	$this->Location_Model->update($data,$address);
         } 
         $this->db->where("status", false);
         $query = $this->db->get("location"); 
         $data['records'] = $query->result(); 
         $this->load->helper('form');
         $this->load->view('Location_view',$data); 
      } 
   } 
?>