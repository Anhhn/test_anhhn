
CREATE DATABASE IF NOT EXISTS `cimaps` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `cimaps`;

DROP TABLE IF EXISTS `location`;
CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `note` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

